package repository

import (
	"context"
	userError "events/domain/user/error"
	"events/domain/user/model"
	"fmt"
	"github.com/apex/log"
	"github.com/cockroachdb/cockroach-go/v2/crdb/crdbpgx"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
)

type UserCockroachRepository struct {
	Pool *pgxpool.Pool
}

func (u UserCockroachRepository) FindById(id int) (*model.User, error) {
	var user model.User

	db, dbErr := u.Pool.Acquire(context.Background())
	if dbErr != nil {
		log.Fatal(dbErr.Error())

		return nil, dbErr
	}

	defer db.Release()

	query := `
	SELECT
		users.id,
		users.created_at,
		users.ip,
		users.country,
		users.system,
		users.device,
		users.deleted_at,
		session.points,
		session.level_id
	FROM
		users
	LEFT JOIN session ON users.id = session.user_id
	WHERE users.id = $1 AND users.deleted_at IS NULL;
	`

	// @TODO User session is not returned as expected, points and level always returns 0
	queryErr := db.QueryRow(context.Background(), query, id).Scan(
		&user.ID, &user.CreatedAt, &user.Ip, &user.Country, &user.System, &user.Device, &user.DeletedAt,
		&user.Session.Points, &user.Session.LevelId,
	)

	if queryErr == pgx.ErrNoRows {
		return &user, &userError.NotFound{UserId: id}
	}

	if queryErr != nil {
		log.Error(queryErr.Error())
	}

	return &user, nil
}

func (u UserCockroachRepository) Create(user model.User) (*model.User, error) {
	var userId int

	db, dbErr := u.Pool.Acquire(context.Background())
	if dbErr != nil {
		log.Error(dbErr.Error())

		return nil, dbErr
	}

	defer db.Release()

	// Run a transfer in a transaction
	err := crdbpgx.ExecuteTx(context.Background(), db, pgx.TxOptions{}, func(tx pgx.Tx) error {
		if err := tx.QueryRow(context.Background(),
			"INSERT INTO users (ip, country, system, device) VALUES ($1, $2, $3, $4) RETURNING id",
			user.Ip, user.Country, user.System, user.Device).Scan(&userId); err != nil {
			log.Error(err.Error())

			return err
		}

		if _, err := tx.Exec(context.Background(),
			"INSERT INTO session (user_id, points, level_id) VALUES ($1, $2, $3)",
			userId, user.Session.Points, user.Session.LevelId); err != nil {
			log.Error(err.Error())

			return err
		}

		return nil
	})

	if err == nil {
		log.Debugf("Created a user with ID: %d", userId)
	} else {
		log.Error(err.Error())
	}

	user.ID = userId

	return &user, err
}

func (u UserCockroachRepository) UpdateSession(userId int, session *model.Session) error {
	db, dbErr := u.Pool.Acquire(context.Background())
	if dbErr != nil {
		log.Errorf("Failed to acquire connection from pool", dbErr.Error())

		return dbErr
	}

	defer db.Release()

	query := fmt.Sprintf(`
		UPDATE session
			SET points = %d, level_id = %d
		FROM users
		WHERE
			users.id = %d AND
			users.deleted_at IS NULL`,
		session.Points, session.LevelId, userId)

	result, err := db.Exec(context.Background(),query)

	// If error exists
	if err != nil {
		log.Fatal(err.Error())

		return err
	}

	// If user and session deleted or not found
	if result.RowsAffected() == 0 {
		return &userError.NotFound{UserId: userId}
	}

	// If no errors
	return nil
}

func (u UserCockroachRepository) Delete(userId int) error {
	db, dbErr := u.Pool.Acquire(context.Background())
	if dbErr != nil {
		log.Errorf("Failed to acquire connection from pool: %s", dbErr.Error())

		return dbErr
	}

	defer db.Release()

	result, err := db.Exec(context.Background(),
		`UPDATE users
			SET deleted_at = now()
			WHERE users.id = $1 AND deleted_at IS NULL`, userId)

	// If error exists
	if err != nil {
		log.Error(err.Error())

		return err
	}

	// If user and session already deleted or not found
	if result.RowsAffected() == 0 {
		return &userError.NotFound{UserId: userId}
	}

	// If no errors
	return nil
}

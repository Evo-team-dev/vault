package main

import (
	"events/application/config"
	"events/application/database"
	"events/application/debug"
	"events/application/routes"
	"events/application/validator"
	"fmt"
	"github.com/labstack/echo/v4"
)

func main() {
	// Init configuration
	config.Init()

	// Init framework
	e := echo.New()

	//Init validation
	validator.Init(e)

	// Init routes
	routes.Init(e)

	// Init database
	database.Init()

	// Start server
	address := fmt.Sprintf(":%s", config.App.AppPort)
	err := e.Start(address)
	if err != nil {
		debug.DD(err.Error())
	}
}
package command

import (
	"events/domain/user/model"
	"events/domain/user/repository"
)

type GetUser struct {
	Id int
}

func (g GetUser) Handle(userRepository repository.UserRepository) (*model.User, error) {
	return userRepository.FindById(g.Id)
}

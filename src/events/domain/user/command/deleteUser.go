package command

import (
	"events/domain/user/repository"
)

type DeleteUserCommand struct {
	Id int
}

func (d DeleteUserCommand) Handle(userRepository repository.UserRepository) error {
	return userRepository.Delete(d.Id)
}

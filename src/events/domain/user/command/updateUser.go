package command

import (
	models "events/domain/user/model"
	"events/domain/user/repository"
)

type UpdateUser struct {
	UserId    int
	Session models.Session
}

func (u *UpdateUser) Handle(userRepository repository.UserRepository) error {
	foundUser, err := userRepository.FindById(u.UserId)
	if err != nil {
		return err
	}

	u.Session.Points = u.Session.Points + foundUser.Session.Points

	return userRepository.UpdateSession(u.UserId, &u.Session)
}

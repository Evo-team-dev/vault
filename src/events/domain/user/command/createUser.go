package command

import (
	models "events/domain/user/model"
	"events/domain/user/repository"
)

type CreateUserCommand struct {
	Ip string
	Country string
	System string
	Device string
}

func (c CreateUserCommand) Handle(userRepository repository.UserRepository) (*models.User, error) {
	user := models.User{
		Ip: c.Ip,
		Country: c.Country,
		System: c.System,
		Device: c.Device,
	}

	// level 0 means that user has not started the game
	// level 1 means that user has started the game, but has not finished level 1
	user.Session = models.Session{
		LevelId: 0,
		Points: 0,
	}

	return userRepository.Create(user)
}

package repository

import (
	"events/domain/user/model"
)

type UserRepository interface {
	Create(user model.User) (*model.User, error)
	FindById(id int) (*model.User, error)
	UpdateSession(id int, session *model.Session) error
	Delete(id int) error
}
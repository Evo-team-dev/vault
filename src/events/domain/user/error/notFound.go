package user

import "fmt"

type NotFound struct {
	UserId int
}

func (e *NotFound) Error() string {
	return fmt.Sprintf("User id - %v not found with requested criterias", e.UserId)
}
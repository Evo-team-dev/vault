package model

import (
	"github.com/jackc/pgtype"
	"time"
)

type User struct {
	ID        int
	CreatedAt time.Time
	Ip        string
	Country   string
	System    string
	Device    string
	DeletedAt *pgtype.Timestamptz
	Session   Session
}

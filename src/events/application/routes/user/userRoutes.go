package user

import (
	"events/application/database"
	"events/application/middleware"
	request "events/application/request/user"
	coreResponse "events/application/response/core"
	response "events/application/response/user"
	"events/domain/user/command"
	userError "events/domain/user/error"
	"events/domain/user/model"
	"events/infrastructure/user/repository"
	"github.com/apex/log"
	"github.com/labstack/echo/v4"
	"net/http"
	"strconv"
)

func InitUserRoutes(baseGroup *echo.Group) {
	userGroup := baseGroup.Group("/user")
	userGroup.POST("", createUser)
	userGroup.GET("/:id", getUser, middleware.RouteAuthentication())
	userGroup.DELETE("/:id", deleteUser, middleware.RouteAuthentication())
	userGroup.PUT("/:id/session", updateUserSession, middleware.RouteAuthentication())
}

func createUser(c echo.Context) error {
	// Get json request body
	createUserRequest := new(request.CreateUserRequest)
	err := c.Bind(createUserRequest)
	if err != nil {
		return err
	}

	// Create command
	createUser := command.CreateUserCommand{
		Ip: createUserRequest.Ip,
		Country: createUserRequest.Country,
		System: createUserRequest.System,
		Device: createUserRequest.Device,
	}

	// Get repository
	userCockroachRepository := repository.UserCockroachRepository{Pool: database.Manager()}
	result, commandError := createUser.Handle(userCockroachRepository)

	if commandError != nil {
		return c.NoContent(http.StatusBadRequest)
	}

	return c.JSON(http.StatusCreated, response.CreateUserResponse{
		Id:    strconv.Itoa(result.ID),
	})
}

func getUser(c echo.Context) error {
	// Get json request body
	id, _ := strconv.Atoi(c.Param("id"))

	// Set command parameters
	getUser := command.GetUser{Id: id}

	// Inject command dependencies
	userCockroachRepository := repository.UserCockroachRepository{Pool: database.Manager()}

	// Execute command
	user, err := getUser.Handle(userCockroachRepository)

	// If user is empty or there is an error
	if user == nil || err != nil {
		// If user not found
		if errorMessage, ok := err.(*userError.NotFound); ok {
			return c.JSON(http.StatusNotFound, coreResponse.ErrorResponse{Error: errorMessage.Error()})
		}

		return c.NoContent(http.StatusBadRequest)
	}

	jsonResponse := response.GetUser{
		Id: strconv.Itoa(user.ID),
		GetUserSession: response.GetUserSession{
			Points:  user.Session.Points,
			LevelId: user.Session.LevelId,
		},
	}

	return c.JSON(http.StatusOK, jsonResponse)
}

func updateUserSession(c echo.Context) error {
	// Get user id
	id, _ := strconv.Atoi(c.Param("id"))

	// Get json request body
	updateUserRequest := new(request.UpdateUserRequest)
	err := c.Bind(updateUserRequest)
	if err != nil {
		log.Error(err.Error())
		return c.NoContent(http.StatusInternalServerError)
	}

	if err = c.Validate(updateUserRequest); err != nil {
		log.Error(err.Error())
		return c.NoContent(http.StatusBadRequest)
	}

	// Update the user
	userSession := model.Session{
		LevelId:   updateUserRequest.LevelId,
		Points:    *updateUserRequest.Points,
	}

	updateUser := command.UpdateUser{UserId: id, Session: userSession}
	err = updateUser.Handle(repository.UserCockroachRepository{Pool: database.Manager()})

	if errorMessage, ok := err.(*userError.NotFound); ok {
		return c.JSON(http.StatusNotFound, coreResponse.ErrorResponse{Error: errorMessage.Error()})
	} else if err != nil {
		log.Error(err.Error())
		return c.NoContent(http.StatusInternalServerError)
	} else {
		jsonResponse := response.UpdateUserSession{
			LevelId: userSession.LevelId,
			Points: userSession.Points,
		}

		return c.JSON(http.StatusOK, jsonResponse)
	}
}

func deleteUser(c echo.Context) error {
	// Get json request body
	id, _ := strconv.Atoi(c.Param("id"))

	// Set command parameters
	deleteUser := command.DeleteUserCommand{Id: id}

	// Inject command dependencies
	userCockroachRepository := repository.UserCockroachRepository{Pool: database.Manager()}

	// Execute command
	err := deleteUser.Handle(userCockroachRepository)

	if errorMessage, ok := err.(*userError.NotFound); ok {
		return c.JSON(http.StatusNotFound, coreResponse.ErrorResponse{Error: errorMessage.Error()})
	} else if err != nil {
		log.Error(err.Error())
		return c.NoContent(http.StatusInternalServerError)
	} else {
		return c.NoContent(http.StatusOK)
	}
}
package routes

import (
	"events/application/routes/user"
	"fmt"
	"github.com/labstack/echo/v4"
)

func Init(e *echo.Echo) {
	const prefix = "api"
	const version = "v1"

	baseGroup := e.Group(fmt.Sprintf("/%s/%s", prefix, version))

	// Init user routes
	user.InitUserRoutes(baseGroup)
}
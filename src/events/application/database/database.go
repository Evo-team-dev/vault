package database

import (
	"context"
	"events/application/config"
	"fmt"
	"github.com/jackc/pgx/v4/pgxpool"
	"log"
)

var pool *pgxpool.Pool

func Init() {
	url := fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=%s",
		config.App.DbUser,
		config.App.DbPassword,
		config.App.DbHost,
		config.App.DbPort,
		config.App.DbName,
		config.App.DbSecure,
	)

	pgxConfig, pgxErr := pgxpool.ParseConfig(url)
	if pgxErr != nil {
		log.Fatal("error parsing db config: ", pgxErr)
	}

	pgxConfig.MinConns = 1
	pgxConfig.MaxConns = 4

	pgxPool, poolEr := pgxpool.ConnectConfig(context.Background(), pgxConfig)
	if poolEr != nil {
		log.Fatal("error connection to db: ", poolEr)
	}

	pool = pgxPool

	//defer pool.Close()

	log.Print("Connected to the database")

	dbCon, err := pool.Acquire(context.Background())
	if err != nil {
		return
	}

	if _, err := dbCon.Exec(context.Background(),
		"create table if not exists users(" +
		"id bigserial," +
		"created_at timestamp default now()," +
		"ip string," +
		"country string," +
		"system string," +
		"device string," +
		"deleted_at timestamp default null," +
		"PRIMARY KEY (id))"); err != nil {
		log.Fatal(err)
	}

	if _, err := dbCon.Exec(context.Background(),
		"create table if not exists session(" +
			"user_id bigserial," +
			"points int," +
			"level_id int," +
			"FOREIGN KEY (user_id) REFERENCES users (id))"); err != nil {
		log.Fatal(err)
	}

	dbCon.Release()
}

func Manager() *pgxpool.Pool {
	return pool
}

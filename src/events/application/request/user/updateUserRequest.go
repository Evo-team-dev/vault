package request

type UpdateUserRequest struct {
	LevelId  int `json:"level_id" validate:"required"`
	Points  *int `json:"points" validate:"required"`
	// By specifying pointer it is possible to set that include also 0 values
	// Without pointer it will return bad request
}
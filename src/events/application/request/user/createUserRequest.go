package request

type CreateUserRequest struct {
	Ip  string `json:"ip" validate:"optional"`
	Country  string `json:"country" validate:"optional"`
	System  string `json:"system" validate:"optional"`
	Device  string `json:"device" validate:"optional"`
}
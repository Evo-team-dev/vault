package config

import (
	"os"
)

var App appConfig

type appConfig struct {
	AppEnv string
	AppPort string

	DbHost string
	DbPort string
	DbName string
	DbUser string
	DbPassword string
	DbSecure string

	JWTSecret string
}


func Init() {
	App = appConfig{
		AppEnv:  os.Getenv("APP_ENV"),
		AppPort: os.Getenv("PORT"),

		DbHost:     os.Getenv("COCKROACH_HOST"),
		DbPort:     os.Getenv("COCKROACH_PORT"),
		DbName:     os.Getenv("COCKROACH_DB"),
		DbUser:     os.Getenv("COCKROACH_USER"),
		DbPassword: os.Getenv("COCKROACH_PASSWORD"),
		DbSecure: os.Getenv("COCKROACH_SECURE"),

		JWTSecret: os.Getenv("JWT_SECRET"),
	}
}

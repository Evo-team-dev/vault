package middleware

import (
	"events/application/config"
	"github.com/labstack/echo/v4"
	eMiddleware "github.com/labstack/echo/v4/middleware"
)

func RouteAuthentication() echo.MiddlewareFunc {
	return eMiddleware.KeyAuth(func(key string, c echo.Context) (bool, error) {
		return key == config.App.JWTSecret, nil
	})
}

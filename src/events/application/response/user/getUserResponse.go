package response

type GetUser struct {
	Id             string `json:"id"`
	GetUserSession `json:"session"`
}

type GetUserSession struct{
	LevelId int `json:"level_id"`
	Points int `json:"points"`
}


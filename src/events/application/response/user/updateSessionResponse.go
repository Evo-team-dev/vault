package response

type UpdateUserSession struct{
	LevelId int `json:"level_id"`
	Points int `json:"points"`
}


=== LOCAL ===
To setup database it is required to create it manually by authenticating in cockroachdb

=== HEROKU ===
Heroku login:<br />
`heroku login`<br />
`heroku container:login`

Build production build:<br />
`docker build -t vault/stardust -f Dockerfile.prod .`

Tag the image:<br />
`docker tag {{IMAGE_ID}} registry.heroku.com/vault-stardust/web`

`docker push registry.heroku.com/vault-stardust/web`

Deploy to Heroku:<br />
`heroku container:release web -a vault-stardust`